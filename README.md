# Adblock Plus for Safari (iOS)

## Building

### Requirements

- [Xcode 14](https://developer.apple.com/xcode/)
- [SwiftLint](https://github.com/realm/SwiftLint) (optional)

### Building in Xcode
*NOTE*: If you do not wish to make use of Firebase for Analytics & Crash Reporting, remove the references from the project before building.

1. Copy the file `ABP-Secret-API-Env-Vars.sh` (available internally, sample file here(https://gitlab.com/eyeo/sandbox/adblock-plus-for-safari/snippets/1865852)) into the same directory as `AdblockPlusSafari.xcodeproj`.
2. Open `AdblockPlusSafari.xcodeproj` in Xcode.
3. Build & Run the project.