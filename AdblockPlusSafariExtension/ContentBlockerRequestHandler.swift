/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import MobileCoreServices
import UIKit

class ContentBlockerRequestHandler: NSObject, NSExtensionRequestHandling {

    /// Checks if Acceptable Ads setting is enabled.
    ///
    /// - Returns: Blocklist as per stored user settings.
    private func getBlocklistName() -> String {

        let container = Constants.groupIdentifier
        let groupDefaults = UserDefaults(suiteName: container)

        // Return the test filterlist if user disables the blockAds Switch. 
        let blockAdsEnabled = groupDefaults?.object(forKey: Constants.blockAdsEnabled) as? Bool
        if blockAdsEnabled == false {
            return Constants.sharedFilterlistTest
        }

        // Used .object rather than .bool as .bool returns false if key doesnt exist, rather than nil.
        guard let acceptableAdsEnabled = groupDefaults?.object(forKey: Constants.acceptableAdsEnabled) as? Bool else {
            return Constants.sharedFilterlistPlusExceptionRules
        }
        switch acceptableAdsEnabled {
        case true:
            return Constants.sharedFilterlistPlusExceptionRules
        case false:
            return Constants.sharedFilterlist
        }
    }

    func beginRequest(with context: NSExtensionContext) {
        let fileManager = FileManager.default
        let blocklistName = getBlocklistName()
        let blocklistURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: Constants.groupIdentifier)
        let blocklistPath = URL(string: "\(blocklistName).json", relativeTo: blocklistURL)

        if fileManager.fileExists(atPath: blocklistPath?.path ?? "") {
            let attachment = NSItemProvider(contentsOf: blocklistPath)!
            let item = NSExtensionItem()
            item.attachments = [attachment]
            context.completeRequest(returningItems: [item], completionHandler: nil)
        } else {
            if #available(iOS 15, *) {
                let attachment = NSItemProvider(contentsOf: Bundle.main.url(forResource: "easylist+safari15", withExtension: "json"))!
                let item = NSExtensionItem()
                item.attachments = [attachment]
                context.completeRequest(returningItems: [item], completionHandler: nil)
            } else {
                let attachment = NSItemProvider(contentsOf: Bundle.main.url(forResource: "easylist_min_content_blocker", withExtension: "json"))!
                let item = NSExtensionItem()
                item.attachments = [attachment]
                context.completeRequest(returningItems: [item], completionHandler: nil)
            }
        }
    }
}
