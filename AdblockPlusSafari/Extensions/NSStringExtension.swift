/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

extension NSString {
    /// - Returns: String with extra domain formatting stripped out
    func allowlistHostname() -> String? {
        // Convert to lower case
        let input = self.lowercased
        // Trim hostname
        var hostname = input.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        // Prepend scheme if needed
        if !hostname.hasPrefix("http://") && !hostname.hasPrefix("https://") {
            hostname = "http://" + hostname
        }
        // Try to get host from URL
        hostname = URL(string: hostname)?.host ?? ""
        if hostname.count == 0 {
            hostname = self as String
            return nil
        }
        // Remove not allowed characters
        let invalidCharSet = CharacterSet(charactersIn: "\\|()[{^$*?<>")
        hostname = hostname.components(separatedBy: invalidCharSet).joined()

        // Remove www prefix
        hostname = hostname.replacingOccurrences(of: "www.", with: "")

        return hostname
    }
}
