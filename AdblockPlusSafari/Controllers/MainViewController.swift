/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Markup
import SafariServices
import UIKit

class MainViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var blockAdsView: UIView!
    @IBOutlet weak var exceptionsView: UIView!
    @IBOutlet weak var blockAdsSwitch: UISwitch!
    @IBOutlet weak var blockAdsActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var messageViewLabel: UILabel!
    @IBOutlet weak var settingsButton: UIBarButtonItem!
    @IBOutlet weak var exceptionsButton: UIButton!

    fileprivate let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)

    // MARK: - IBActions

    @IBAction func didPressMessageViewButton(_ sender: Any) {
        let animations = {
            self.messageView.isHidden = true
            self.messageView.alpha = 0
        }

        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: animations,
                       completion: { _ in
        })
        groupDefaults?.set(true, forKey: Constants.messageCardDismissed)
    }
    @IBAction func blockAdsSwitchToggle(_ sender: UISwitch) {
        self.groupDefaults?.set(sender.isOn, forKey: Constants.blockAdsEnabled)
        setLoadingState(asEnabled: true)
        ContentBlockerManager.shared().reloadContentBlocker()
        switch sender.isOn {
        case true:
            FirebaseWrapper.sendEvent(event: AnalyticEvents.mainBlockAdsSwitchActive)
        case false:
            FirebaseWrapper.sendEvent(event: AnalyticEvents.mainBlockAdsSwitchInactive)
        }
    }

    @IBAction func didPressSettingsButton(_ sender: Any) {
        self.performSegue(withIdentifier: "pushToSettingsView", sender: nil)
    }

    @IBAction func didPressExceptionsButton(_ sender: Any) {
        self.performSegue(withIdentifier: "pushToExceptionsView", sender: nil)
    }

    // MARK: - View Logic

    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyles()
        applyAttributedText()
        checkForFirstLaunch()
        checkMessageViewStatus()
        setBlockAdsSwitchState()
        // Added a short delay to reload content blocker from when app launches to ensure that
        // any updates (such as copying filter lists and applying the allowlist) doesn't clash.
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.setLoadingState(asEnabled: true)
            ContentBlockerManager.shared().reloadContentBlocker()
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupNotificationObservers()

        // We also check for first launch here in order to determine whether the content blocker is
        // enabled in iOS Safari Settings. If not, when a user reaches this screen, a modal view will
        // appear.
        checkForFirstLaunch()

        FirebaseWrapper.sendEvent(event: AnalyticEvents.mainShown)
        ContentBlockerManager.shared().delegate = self
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeNotificationObservers()
        FirebaseWrapper.sendEvent(event: AnalyticEvents.mainDismissed)
    }

    fileprivate func setBlockAdsSwitchState() {
        guard let blockAdsEnabled = self.groupDefaults?.object(forKey: Constants.blockAdsEnabled) as? Bool else {
            self.groupDefaults?.set(true, forKey: Constants.blockAdsEnabled)
            return
        }
        blockAdsSwitch.setOn(blockAdsEnabled, animated: false)
    }

    fileprivate func applyAttributedText() {
        // Markup Renderer
        let instructionRenderer = MarkupRenderer(baseFont: .systemFont(ofSize: 14))

        // Message View Label.
        // swiftlint:disable:next line_length
        let messageViewLabelText = "Annoying ads are always blocked while nonintrusive ads are displayed by default. You can change this setting at any time.\n\n*Tap on Exceptions > Acceptable Ads*".localized

        // Apply attributed strings.
        messageViewLabel.attributedText = instructionRenderer.render(text: messageViewLabelText)
    }

    fileprivate func applyStyles() {
        // Apply border color for cards
        messageView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor
        blockAdsView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor
        exceptionsView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor

        // Device specific adjustments.
        // swiftlint:disable:next discouraged_direct_init
        let userInterfaceIdiom = UIDevice().userInterfaceIdiom
        if  userInterfaceIdiom == .phone {
            let iPhone5Height = 568
            if Int(UIScreen.main.bounds.size.height) == iPhone5Height {
                // iPhone 5/SE Adjustments
                navigationController?.navigationBar.prefersLargeTitles = false
            }
        }
    }

    /// Dismisses messageView if previously dismissed.
    fileprivate func checkMessageViewStatus() {
        let messageCardDismissedStatus = groupDefaults?.bool(forKey: Constants.messageCardDismissed) ?? false
        if messageCardDismissedStatus == true {
            messageView.isHidden = true
        }
    }

    // Apply custom colors when app switches between dark mode. 
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        messageView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor
        blockAdsView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor
        exceptionsView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor
    }

    /// Starts animation spinner and disables UIButtons
    /// - Parameter state: Passing true will enable loading state or false will disable the loading state.
    fileprivate func setLoadingState(asEnabled state: Bool) {
        DispatchQueue.main.async {
            self.blockAdsSwitch.isHidden = state
            self.settingsButton.isEnabled = !state
            self.exceptionsButton.isEnabled = !state

            switch state {
            case true:
                self.blockAdsActivityIndicator.startAnimating()
                self.exceptionsButton.layer.backgroundColor = UIColor.customColor(.abpPureWhiteTrans)?.cgColor
            default:
                self.blockAdsActivityIndicator.stopAnimating()
                self.exceptionsButton.layer.backgroundColor = nil
            }
        }
    }

    // MARK: - First Run Logic

    func checkForFirstLaunch() {
        let hasRun = groupDefaults?.bool(forKey: Constants.hasRun) ?? false
        let installedAppVersion = groupDefaults?.string(forKey: Constants.storedAppVersion)
        let currentVersionNumber = Constants.currentVersionNumber

        // Force onboarding if hasRun == false
        // if hasRun == true, then will continue to show onboarding if CB is disabled in settings
        showOnboarding(force: !hasRun)

        // If hasRun == false, Onboarding logic will be called & Filter lists will be copied to shared container.
        // If Installed App Version != Current Version, local Filter lists will be copied to shared container.
        if hasRun == false {
            groupDefaults?.set(currentVersionNumber, forKey: Constants.storedAppVersion)
            FilterlistManager.copyFilterListsToSharedContainer()
            migrateFromLegacyApp()
        } else if installedAppVersion != currentVersionNumber {
            groupDefaults?.set(currentVersionNumber, forKey: Constants.storedAppVersion)
            // copy new filter lists
            FilterlistManager.copyFilterListsToSharedContainer()
            // Apply Allowlist
            let allowlistItemDataSource = AllowlistItemDataSource()
            let allowlistArray = allowlistItemDataSource.getAllowlistArray()
            if !allowlistArray.isEmpty {
                let allowlistManager = AllowlistManager()
                allowlistManager.applyAllowlist(allowlistArray)
            }
        }
    }

    fileprivate func migrateFromLegacyApp() {
        // Migrate information from ABP 1.x
        // We use .object rather than .bool, as .bool returns false in lieu of a nil value.
        let blockAdsEnabled = groupDefaults?.object(forKey: Constants.legacyBlockAdsEnabled) as? Bool ?? true
        let exceptionsEnabled = groupDefaults?.object(forKey: Constants.legacyAcceptableAdsEnabled) ?? true
        let allowlistArray = groupDefaults?.array(forKey: Constants.legacyAllowlistArray) as? [String] ?? []

        // Migrate legacy settings to new user defaults keys
        groupDefaults?.set(blockAdsEnabled, forKey: Constants.blockAdsEnabled)
        groupDefaults?.set(exceptionsEnabled, forKey: Constants.acceptableAdsEnabled)
        groupDefaults?.set(allowlistArray, forKey: Constants.allowlistArray)

        // Update UI
        setBlockAdsSwitchState()

        // Apply allowlist if array isn't empty
        if !allowlistArray.isEmpty {
            let allowlistManager = AllowlistManager()
            allowlistManager.applyAllowlist(allowlistArray)
        }
    }

    /// Presents OnboardingViewController
    /// - Parameter force: Forces onboarding to be shown if true
    fileprivate func showOnboarding(force: Bool) {
        let contentBlockerState = ContentBlockerManager.shared().getContentBlockerState()
        // Show onboarding if CB State is disabled or if force flag is passed as true.
        if (contentBlockerState == false || force == true) && onboardingIsPresented() == false {
            let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
            // swiftlint:disable:next force_cast
            let onboardingViewController = storyboard.instantiateViewController(withIdentifier: "Onboarding") as! OnboardingViewController
            onboardingViewController.isModalInPopover = true
            // Device specific adjustments.
            // swiftlint:disable:next discouraged_direct_init
            let userInterfaceIdiom = UIDevice().userInterfaceIdiom
            if  userInterfaceIdiom == .phone {
                let iPhone6Height = 667
                if Int(UIScreen.main.bounds.size.height) <= iPhone6Height {
                    // iPhone 6,7,8,SE Adjustments
                    onboardingViewController.modalPresentationStyle = .fullScreen
                }
            }
            self.parent?.present(onboardingViewController, animated: true, completion: nil)
        }
    }

    /// Checks whether OnboardingViewController is being presented
    fileprivate func onboardingIsPresented() -> Bool {
        let presented: Bool? = self.presentedViewController?.isKind(of: OnboardingViewController.self)
        if presented ?? false {
            return true
        }
        return false
    }

    // MARK: - Notification Observers

    fileprivate func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
    }

    fileprivate func removeNotificationObservers() {
        NotificationCenter.default.removeObserver(self,
                                                  name: UIApplication.didBecomeActiveNotification,
                                                  object: nil)
    }

    @objc
    fileprivate func applicationDidBecomeActive() {
        checkForFirstLaunch()
    }
}

// MARK: - Delegate Extension

extension MainViewController: ContentBlockerManagerDelegate {
    func contentBlockerStateDidFail(error: Error) {
        setLoadingState(asEnabled: false)
    }

    func contentBlockerUpdateDidFail(error: Error) {
        setLoadingState(asEnabled: false)
    }

    func contentBlockerUpdateDidSucceed() {
        setLoadingState(asEnabled: false)
    }

}
