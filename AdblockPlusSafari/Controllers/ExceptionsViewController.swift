/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import UIKit

class ExceptionsViewController: UITableViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var backButton: UINavigationItem!
    @IBOutlet weak var aaSwitch: UISwitch!
    @IBOutlet weak var aaActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var allowlistWebsitesLabel: UILabel!

    /// Shared UserDefaults
    private let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)

    // MARK: - IBActions
    @IBAction func aaSwitchDidChange(_ sender: UISwitch) {
        setLoadingState(asEnabled: true)
        groupDefaults?.set(aaSwitch.isOn, forKey: Constants.acceptableAdsEnabled)
        ContentBlockerManager.shared().reloadContentBlocker()
        switch sender.isOn {
        case true:
            FirebaseWrapper.sendEvent(event: AnalyticEvents.exceptionsAASwitchActive)
        case false:
            FirebaseWrapper.sendEvent(event: AnalyticEvents.exceptionsAASwitchInactive)
        }
    }

    // MARK: - View Logic
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 53.0
        tableView.rowHeight = UITableView.automaticDimension
        setAcceptableAdsSwitchState(theSwitch: aaSwitch)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        FirebaseWrapper.sendEvent(event: AnalyticEvents.exceptionsShown)
        ContentBlockerManager.shared().delegate = self
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        FirebaseWrapper.sendEvent(event: AnalyticEvents.exceptionsDismissed)
    }

    /// Starts animation spinner and disables UIButtons
    /// - Parameter state: Passing true will enable loading state or false will disable the loading state.
    fileprivate func setLoadingState(asEnabled state: Bool) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isUserInteractionEnabled = !state
            self.tableView.allowsSelection = !state
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = !state
            self.aaSwitch.isHidden = state
            switch state {
            case true:
                self.aaActivityIndicator.startAnimating()
                self.allowlistWebsitesLabel.alpha = 0.5
                self.navigationController?.navigationBar.alpha = 0.5
            default:
                self.aaActivityIndicator.stopAnimating()
                self.allowlistWebsitesLabel.alpha = 1
                self.navigationController?.navigationBar.alpha = 1
            }
        }
    }

    /// When app is launched, this method checks NSUserDefaults and sets the checkbox as per user preference.
    ///
    /// - Parameter theSwitch: The UISwitch to update state of.
    private func setAcceptableAdsSwitchState(theSwitch: UISwitch) {
        DispatchQueue.main.async {
            guard let acceptableAdsEnabled = self.groupDefaults?.object(forKey: Constants.acceptableAdsEnabled) as? Bool else {
                self.groupDefaults?.set(true, forKey: Constants.acceptableAdsEnabled)
                theSwitch.setOn(true, animated: true)
                return
            }

            theSwitch.setOn(acceptableAdsEnabled, animated: true)
        }
    }

    // MARK: - Table View Data Source / Delegate
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0 {
            return false
        }

        return true
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 1 {
            self.performSegue(withIdentifier: "pushToAllowlistView", sender: nil)
        }
    }
}

extension ExceptionsViewController: ContentBlockerManagerDelegate {
    func contentBlockerStateDidFail(error: Error) {
        setLoadingState(asEnabled: false)
    }

    func contentBlockerUpdateDidFail(error: Error) {
        setLoadingState(asEnabled: false)
    }

    func contentBlockerUpdateDidSucceed() {
        // Content blocker did update. Verify state of AA.
        setAcceptableAdsSwitchState(theSwitch: aaSwitch)
        setLoadingState(asEnabled: false)
    }
}
