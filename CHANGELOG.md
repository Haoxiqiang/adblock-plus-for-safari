# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.1] - 2022-09-20
### Added
* Safari Extension Sync support
* Updated to Xcode 14
* [Updated Filter Lists](https://gitlab.com/eyeo/filterlists/contentblockerlists/-/commit/e774af0cc1eaf9623c445fb4de8356b1fbf8b8ae)

## [2.1] - 2022-06-14
### Added
* Updated strings + onboarding video to provide correct instructions on iOS 15+
* [Updated Filter Lists to new Safari 15 variants on iOS 15+](https://gitlab.com/eyeo/filterlists/contentblockerlists/-/commit/f7d67ef0bd55bcf5ce0ef33eaf17273d7f374d90)

## [2.0.14] - 2022-01-25
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/c1027db16b5082a2587b2bcc8478756134178423)

## [2.0.13] - 2022-01-11
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/6808ee13e1bad1c3123bff32848ce41e767b12c1)

## [2.0.12] - 2021-12-02
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/70714f2d648873d52c3522f4dad339b4384cb725)
### Changed
* Moved Firebase dependancy to SPM from Carthage
* Removed Carthage

## [2.0.11] - 2021-11-07
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/70714f2d648873d52c3522f4dad339b4384cb725)

## [2.0.10] - 2021-09-14
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/04b4dab79a32a9eb44f50a01962233548b65d948)

## [2.0.9] - 2021-06-07
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/d42ec7cdfc5f5bb06096126441e2cf1eb7313e10)

## [2.0.8] - 2021-05-14
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/6454f344a881f3dc65cdc4829ed554e35488b927)

## [2.0.7] - 2021-05-04
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/36ba81a5c972322a1ab25019b3b781295f936f14)
### Changed
* Changed references of `whitelist` to`allowlist`

## [2.0.6] - 2020-12-01
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/10783d3959e48fea1fc358dd8e4420e0d0ee5b3f)

## [2.0.5] - 2020-12-01
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/af8d1d767cfb33e98319901e4649579eda60e0da)

## [2.0.4] - 2020-11-03
### Added
* Updated Filter Lists

## [2.0.3] - 2020-07-18
### Added
* Updated Filter Lists
* Updated French Translation

## [2.0.2] - 2020-06-30
### Added
* Onboarding now presents itself if app is reopened from background
* Minor UI tweaks
* Updated Filter Lists

## [2.0.1] - 2020-02-06
### Added
* Updated bundled filter lists

## [2.0.0] - 2019-12-11
### Added
App rewritten from scratch with the following features:
* A Redesign of the whole application
* Support for Dark Mode (iOS 13 only)
* User controlled domain whitelisting
* Acceptable Ads configuration toggle
* Opt-in Firebase Analyictics and crash reporting
* Safari Action Extension for whitelisting